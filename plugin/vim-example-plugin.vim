" vim-example-plugin
" henrik@stroem.net

if exists("g:loaded_vim-example-plugin")
    finish
endif
let g:loaded_vim-example-plugin = 1

" Exposed commands

command! -nargs=0 DisplayTime call vim-example-plugin#DisplayTime()
command! -nargs=0 DefineWord call vim-example-plugin#DefineWord()
command! -nargs=0 AspellCheck call vim-example-plugin#AspellCheck()

