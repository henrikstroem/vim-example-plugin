function! vim-example-plugin#DisplayTime(...)
    if a:0 > 0 && (a:1 == "d" || a:1 == "t")
        if a:1 == "d"
            echo strftime("%b %d")
        elseif a:1 == "t"
            echo strftime("%H:%M")
        endif
    else
        echo strftime("%b %d %H:%M")
    endif
endfunction


python3 << EOF

import vim
import json, requests

headers = {"accept": "application/json"}
base_url = "https://en.wiktionary.org/api/rest_v1/page/definition/"
url_options = "?redirect=true"

def get_word_definitions(word_to_define):
    response = requests.get(base_url + word_to_define + url_options, header=headers)
    if response.status_code != 200:
        print(response.status_code + ": " + response.reason)
        return
    definition_json = json.loads(response.text)
    for definition_item in definition_json["en"]:
        print(definition_item["partOfSpeech"])
        for definition in definition_item["definitions"]:
            print(" - " + definition["definition"])

EOF

function! vim-example-plugin#DefineWord()
    let cursorWord = expand('<cword>')
    python3 get_word_definition(vim.eval('cursorWord'))
endfunction


function! vim-example-plugin#Aspell()
    let cursorWord = expand('<cword>')
    let aspellSuggestions = system("'echo '" . cursorWord . "' | aspell -a'")
    let aspellSuggestions = substitute(aspellSuggestions, "& .* 0:", "", "g")
    let aspellSuggestions = substitute(aspellSuggestions, ", ", "\n", "g")
    echo aspellSuggestions
endfunction
